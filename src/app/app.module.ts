import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// Components
import { AppComponent } from './app.component';
// Angular Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ArticlesComponent } from './components/articles/articles.component';
import { ClassifiedArticlesComponent } from './components/classified-articles/classified-articles.component';
import { RouterModule, Routes } from '@angular/router';
import {MatCardModule} from '@angular/material/card';


const appRoutes: Routes = [
  { path: 'Articles', component: ArticlesComponent },
  { path: 'ClassifiedArticles', component: ClassifiedArticlesComponent },
  { path: "",
    redirectTo: '/Articles',
    pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ArticlesComponent,
    ClassifiedArticlesComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    MatSliderModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'exTest'),
    MatButtonModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AngularFireModule,
    RouterModule,
    MatCardModule,
      
  ],

  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
